import Ember from 'ember';

export default Ember.Component.extend({
  placeholder: "Card Name",
       
       //set up so that the values in each component will be different.

       setupThings: function() {
         this.set('searchResults', []);
         this.set('requestStarted', false);
       }.on('init'),
       //inputValue is a variable we can use here as it is added on the template.
       keyUp() {
         var inputval = this.get('inputValue');
         var searchResults = this.get('searchResults');
         console.log(searchResults);
         var requestStarted = this.get('requestStarted');
         var uniques;  //horray.  JSHint nazi's strike again..
         if (inputval.length > 3 && searchResults.length === 0 && !requestStarted) {
           this.set('requestStarted', true);
           console.log("ajax request starting");
           Ember.$.ajax({
             url: "https://api.magicthegathering.io/v1/cards?",
             dataType: 'json',
             type: "GET",
             data: {
               name: inputval
             }
           }).then(function (data) {
             data.cards.forEach(function(item) {
               searchResults.pushObject(item.name);
             });

           });

           uniques =  searchResults.filter(function(item, index) {
             return searchResults.indexOf(item) === index;
           });

           console.log(searchResults);

           this.set('requestStarted', false);
         } else {
           uniques =  searchResults.filter(function(item, index) {
             return searchResults.indexOf(item) === index;
           });
           var resultSet = uniques.filter(function (item) {
             return item.indexOf(inputval) !== -1;
           });
           this.set('searchResults', resultSet);

         }
       },
       actions: {
         fillInput(value) {
           this.set('inputValue', value);
           this.set('searchResults', []);
         }
       }
         
           

});
