import Ember from 'ember';

export default Ember.Component.extend({ 
  classNames: ['card'],
       classNameBindings: ['isTapped'],

       //isTapped will be replaced with tapped prop on card
       isTapped: false,
       click() {
         this.toggleProperty("isTapped");
       }

});
