import Ember from 'ember';

export default Ember.Controller.extend({
  queryParams: ["decklist1", "decklist2"],
       decklist1: null,
       decklist2: null,
       /*
       parseDeck(deck) {   

         var card = [];
         var mergedcard = "";
         var tempdeck = deck.split("\n");
         var library = [];
         //console.log("parseDeck called successfully");

         for (var i = 0; i < tempdeck.length; i++) { //for each card in the decklist:
           if (tempdeck[i].length >= 3) {
             card = tempdeck[i].split(" ");  //card is split into its components: 3 Jace, the Mind Sculptor = 3,Jace,,the,Mind,Sculptor
             //if the cardname is more than one word:
             mergedcard = card[1];
             for (var k = 0; k < (card.length - 2); k++) {
               //console.log(card[k+1]);
               mergedcard += (" " + card[k+2]);     //add each word to the card.
               //console.log(mergedcard);
             }
             for(var j = 0; j < parseInt(card[0]); j++) {  //for the number of cards to be added to the deck with this name:
               library.push({name: mergedcard });       
             }
           }
         }

         return library;
       },

*/

       //TODO:  set this up as an afterModel hook!!!!!!!
/*
       filteredModel: Ember.computed('decklist1', 'decklist2', 'model', function() {
         let decklist1 = this.get('decklist1');
         let decklist2 = this.get('decklist2');
         let empty = { library1: [], hand1: [], battlefield1: [], graveyard1: [], exile1: [], scratchpad1: [], library2: [], hand2: [], battlefield2: [], graveyard2: [], exile2: [], scratchpad2: [] };
         if (decklist1 && decklist2) {
           empty.library1 = this.parseDeck(decklist1);
           empty.library2 = this.parseDeck(decklist2);
           return empty;
         } else if(decklist1) {
           empty.library1 = this.parseDeck(decklist1);
           empty.library2 = this.get("model.library2");
           return empty;
           return empty;
         } else if (decklist2) {
           empty.library2 = this.parseDeck(decklist2);
           empty.library1 = this.get("model.library1");
           return empty;
         } else if (model) {
           return this.get('model');
         } else {
           return empty;
         }
       }),
       */
       nowDragging: "",
       showLibrary: false,
       actions: {
         movedCard: function (card, oldIndex, newIndex) {
           console.log("A card was moved to " + this.get('nowDragging'));
           const content = this.get('filteredModel'); 
           content.removeAt(oldIndex);
           content.insertAt(newIndex, card);
           console.log(card);
           console.log(content);
           console.log(this.get('nowDragging'));
           //return content.save();
         },
       setDragging: function (loc) {
         var drag = this.set('nowDragging', loc);
         //drag.save();
       },
       libraryClicked: function(player, data, e ) {
         //just some housekeeping /error checking goodstuff.
         let mod = this.get('model');
         console.log(mod);
         console.log("library clicked");
         console.log("Player:");
         console.log(player);
         console.log("Data:");
         console.log(data);
         console.log("e:");
         console.log(e);
         var lib, hand, card, content;  //jshint, I know my code sucks, but please give me a break!
         //TODO: refactoring to just add the player to the end of the string and get the appropriate thing.


         //Check the value of player passed in from the component.
         //then, place in hand of correct player.

         //TODO: ADD check to see if library is empty.  If it is, halt the process. Prompt to end game?

         var libstring = "model.library" + player;
         lib = this.get(libstring);
         if (lib.length <= 0) {
           //TODO: implement sendEvent
           //this.sendEvent({player: player, description: "Player " + player + " attempted to draw from an empty library."});
         } else {

           if(player === 1) {
             lib = this.get('model.library1');
             hand = this.get('model.hand1');
             card = lib.get('firstObject');
             lib.removeObject(card);
             hand.pushObject(card);
             //this.sendEvent(blabla);
             this.set('model.library1', lib);
             console.log(lib);
             console.log(this.get('model'));
             content = this.get('model');
             //return content.save();

           } else if (player === 2) {
             lib = this.get('model.library2');
             hand = this.get('model.hand2');
             card = lib.get('firstObject');
             lib.removeObject(card);
             hand.pushObject(card);
             this.set('model.library2', lib);
             //this.sendEvent(blabla);
             content = this.get('model');
             //return content.save();
           }
         }
       },
       toggleLibrary: function () {
         var libshown = this.get('showLibrary');
         if (libshown === false) {
           this.set('showLibrary', true);
         } else {
           this.set('showLibrary', false);
         }
       }

       } //end of actions list
});
