import Ember from 'ember';

export default Ember.Controller.extend({
  queryParams: ["decklist1", "decklist2"],
       decklist1: null,
       decklist2: null,
  /*  

  stringifiedDecklists: Ember.computed('model', function () {
    return JSON.stringify(this.get('model'));
  },
  */

  decklist: [],
  parseDeck(deck) {   

    var card = [];
    var mergedcard = "";
    var tempdeck = deck.split("\n");
    var library = [];
    //console.log("parseDeck called successfully");

    for (var i = 0; i < tempdeck.length; i++) { //for each card in the decklist:
      if (tempdeck[i].length >= 3) {
        card = tempdeck[i].split(" ");  //card is split into its components: 3 Jace, the Mind Sculptor = 3,Jace,,the,Mind,Sculptor
        //if the cardname is more than one word:
        mergedcard = card[1];
        for (var k = 0; k < (card.length - 2); k++) {
          //console.log(card[k+1]);
          mergedcard += (" " + card[k+2]);     //add each word to the card.
          //console.log(mergedcard);
        }
        for(var j = 0; j < parseInt(card[0]); j++) {  //for the number of cards to be added to the deck with this name:
          library.push(mergedcard);       
        }
      }
    }

    this.set('decklist', library);
  }

});
