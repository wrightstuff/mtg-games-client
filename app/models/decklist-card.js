import DS from 'ember-data';

export default DS.Model.extend({
  decklist: DS.belongsTo('decklist', { inverse: 'decklist' }),
  qty: DS.attr('number'),
  name: DS.attr('string')
});
