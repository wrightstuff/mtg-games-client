import DS from 'ember-data';

export default DS.Model.extend({
  cards: DS.hasMany('decklist-card',{ inverse: 'decklist-card'} ),
  deckName: DS.attr('string'),
  private: DS.attr('boolean', {default: true}) //feature to be added later
});
