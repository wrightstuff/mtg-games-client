import Ember from 'ember';

export default Ember.Route.extend({
	model() {
		return Ember.RSVP.hash({
			hand1: [ {name: "Island"}, {name: "Counterspell"}, {name: "Snapcaster Mage" }],
					 library1: [ {name: "Island"}, {name: "Counterspell"}, {name: "Snapcaster Mage" }],
					 battlefield1: [ {name: "Island"}, {name: "Counterspell"}, {name: "Snapcaster Mage" }],
					 graveyard1: [ {name: "Counterspell"}, {name: "Snapcaster Mage" }, {name: "Jace, Vryn's Prodigy"}],
					 exile1: [ { name: "Island"}, { name: "Island" }, { name: "Island" }, { name: "Jace, Vryn's Prodigy"}], 
					 scratchpad1: [],

					 hand2: [ { name: "Swamp" }, { name: "Swamp" }, { name: "Swamp" }, { name: "Dark Confidant" } ], 
					 library2: [ { name: "Swamp" }, { name: "Swamp" }, { name: "Swamp" }, { name: "Dark Confidant" } ], 
					 battlefield2: [ { name: "Swamp"}, { name: "Swamp" }, { name: "Swamp" }, { name: "Dark Confidant" } ],
					 graveyard2: [ { name: "Swamp"}, { name: "Swamp" }, { name: "Swamp" }, { name: "Dark Confidant" } ],
					 exile2: [ { name: "Swamp"}, { name: "Swamp" }, { name: "Swamp" }, { name: "Dark Confidant" } ],
					 scratchpad2: []
		});
	},
       
       empty: { library1: [], hand1: [], battlefield1: [], graveyard1: [], exile1: [], scratchpad1: [], library2: [], hand2: [], battlefield2: [], graveyard2: [], exile2: [], scratchpad2: [] },

       afterModel(zones, transition) {
         console.log(zones);
         let decklist1 = transition.queryParams.decklist1;
         console.log(decklist1);
         let decklist2 = transition.queryParams.decklist2;
         if (decklist1 && decklist2) {

           console.log(this.parseDeck(decklist1));
           this.set('empty.library1', this.parseDeck(decklist1));
           this.set('empty.library2', this.parseDeck(decklist2));
           //empty.set('library1', this.parseDeck(decklist1));
           //empty.set('library2', this.parseDeck(decklist2));
           zones = this.get('empty');
           console.log(zones);
           this.transitionTo('game', zones);
           //TODO: CREATE A MATCHUP MODEL (OR JUST BOARDSTATE MODEL?) AND ADD DYNAMIC SEGMENT
         } else if(decklist1) {
           //empty.set('library1', this.parseDeck(decklist1));
           //empty.library2 = zones.library2;
           //zones = empty;
         } else if (decklist2) {
           //empty.library2 = this.parseDeck(decklist2);
           //library 1 not provided in query params, so it is taken from model.
           //empty.library1 = zones.library1;
           //zones = empty;
         } else {
           //zones = empty;
         }
       },

       parseDeck(deck) {   

         var card = [];
         var mergedcard = "";
         var tempdeck = deck.split("\n");
         var library = [];
         //console.log("parseDeck called successfully");

         for (var i = 0; i < tempdeck.length; i++) { //for each card in the decklist:
           if (tempdeck[i].length >= 3) {
             card = tempdeck[i].split(" ");  //card is split into its components: 3 Jace, the Mind Sculptor = 3,Jace,,the,Mind,Sculptor
             //if the cardname is more than one word:
             mergedcard = card[1];
             for (var k = 0; k < (card.length - 2); k++) {
               //console.log(card[k+1]);
               mergedcard += (" " + card[k+2]);     //add each word to the card.
               //console.log(mergedcard);
             }
             for(var j = 0; j < parseInt(card[0]); j++) {  //for the number of cards to be added to the deck with this name:
               library.push({name: mergedcard });       
             }
           }
         }

         //returns array of cards.  (e.g. [{name: "Dark Confidant"}])
         return library;
       },
			 setupController(controller, model) {
				 controller.set('model', model);
			 }
});


//			hand1: [ {name: "Island"}, {name: "Counterspell"}, {name: "Snapcaster Mage" }],
//					 library1: this.queryParams.decklist1 || [],
//					 battlefield1: [ {name: "Island"}, {name: "Counterspell"}, {name: "Snapcaster Mage" }],
//					 graveyard1: [ {name: "Counterspell"}, {name: "Snapcaster Mage" }, {name: "Jace, Vryn's Prodigy"}],
//					 exile1: [ { name: "Island"}, { name: "Island" }, { name: "Island" }, { name: "Jace, Vryn's Prodigy"}], 
//					 scratchpad1: [],
//
//					 hand2: [ { name: "Swamp" }, { name: "Swamp" }, { name: "Swamp" }, { name: "Dark Confidant" } ], 
//					 library2: this.queryParams.decklist2 || [],
//					 battlefield2: [ { name: "Swamp"}, { name: "Swamp" }, { name: "Swamp" }, { name: "Dark Confidant" } ],
//					 graveyard2: [ { name: "Swamp"}, { name: "Swamp" }, { name: "Swamp" }, { name: "Dark Confidant" } ],
//					 exile2: [ { name: "Swamp"}, { name: "Swamp" }, { name: "Swamp" }, { name: "Dark Confidant" } ],
//					 scratchpad2: []
