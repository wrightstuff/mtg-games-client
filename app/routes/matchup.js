import Ember from 'ember';

export default Ember.Route.extend({
  model() {
    return Ember.RSVP.hash({
      decklist1: [{qty: 3, name: "Jace, Vryn's Prodigy"}, {qty: 4, name: "Island"}],
           decklist2: [{qty: 2, name: "Dark Confidant"}, {qty: 5, name: "Swamp"}]
    });
  },
       setupController(controller, model) {
         controller.set('model', model);
       },
       actions: {
         addCard(decklist) {
           decklist.pushObject({qty: "", name: ""});
           //this.get('model').save();
           console.log("Card added");
           //console.log(this.get('stringifiedDecklists'));
         },
         saveDeck(decklist) {
          // this.get('model').save();
           console.log("model saved!");
           console.log(decklist);

         } 
       }
});
