import { test } from 'qunit';
import moduleForAcceptance from 'mtg-games-client/tests/helpers/module-for-acceptance';

moduleForAcceptance('Acceptance | game');

test('visiting /game', function(assert) {
  visit('/game');
  andThen(function() {
    assert.equal(currentURL(), '/game');
  });
});

test('starting game from matchup route will bring up a game using the decklists that are displayed when "Start Game! link is clicked', function(assert) {
  visit('/matchup');
  fillIn(".deck1", "3 Dark Confidant\n3 Swamp");
  click(".start-game");
  andThen(function() {
    assert.equal(currentURL(), "/game?decklist1=3%20Dark%20Confidant%0A3%20Swamp");
  });
});



//TODO: test to see if anything happens when an empty library is clicked.
//todo: ensure "attempted to draw from an empty library" event gets sent when a player attempts to draw from
//an empty library.
