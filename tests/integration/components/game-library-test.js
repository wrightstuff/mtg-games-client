import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('game-library', 'Integration | Component | game library', {
  integration: true
});

test('it renders', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{game-library}}`);

  assert.equal(this.$().text().trim(), '0');

/* not going to bother supporting block usage for now.

  // Template block usage:
  this.render(hbs`
    {{#game-library}}
      template block text
    {{/game-library}}
  `);

  assert.contains(this.$().text().trim(), 'template block text');
  */
});

test('it shows a number that represents the number of cards remaining in the library.', function (assert) {
  this.set('library1', [{name: "Bob"}, {name: "Bob"}, {name: "Swamp"}, {name: "Swamp"}]);
  this.render(hbs`{{game-library cards=library1}}`);
  assert.equal(this.$('.total').text(), 4);
});

/* this needs to happen on the behavior side.

test('it alerts the user if library is clicked and there are no cards in it.', function(assert) {
  this.set('library1', [{name: "Bob"}]);
  this.render(hbs`{{game-library cards=library1}}`);
  this.$(".game-library").click();
  this.$(".game-library").click();
  //assert that a flash message or alert appears on the page
});
*/


