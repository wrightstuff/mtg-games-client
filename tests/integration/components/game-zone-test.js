import { moduleForComponent, test } from 'ember-qunit';
//import { find } from 'ember-native-dom-helpers';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('game-zone', 'Integration | Component | game-zone', {
  integration: true
});


test('it renders', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{game-zone}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#game-zone}}
      template block text
    {{/game-zone}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});


test("Cards that are passed into game-zone are rendered", function(assert) {

	this.set("model", {
		exile1: [
			{
				name: 'This is a fake card name I made for testing'
			}
		]
	});
  this.render(hbs`
    {{#game-zone cards=model.exile1}}
    {{/game-zone}}
  `);

	assert.ok(this.$('.card').text() !== undefined, "Card renders successfully", "Card Fails to render successfully");
});

test("Cards added to zone persist", function(assert) {
	this.set("model", {
		exile1: [
			{
				name: 'This is a fake card name I made for testing'
			}
	],
		graveyard1: [
			{
				name: 'This is a fake card name I made for testing'
			}
		]
	});

	assert.equal(0,1);
});


