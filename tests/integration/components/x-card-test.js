import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';
import { click, find } from 'ember-native-dom-helpers';

moduleForComponent('x-card', 'Integration | Component | x card', {
  integration: true
});

test('it renders', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{x-card}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#x-card}}
      template block text
    {{/x-card}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});

//pending tests

test("card visually turned on click", function(assert) {

  this.render(hbs`
    {{#x-card}}
      template block text
    {{/x-card}}
  `);

	click('.card');


	assert.equal(find('.card'), find('.is-tapped'));
	
});


