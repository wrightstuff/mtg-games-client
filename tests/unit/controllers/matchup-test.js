import { moduleFor, test } from 'ember-qunit';

moduleFor('controller:matchup', 'Unit | Controller | matchup', {
  // Specify the other units that are required for this test.
  // needs: ['controller:foo']
});

// Replace this with your real tests.
test('it exists', function(assert) {
  let controller = this.subject();
  assert.ok(controller);
});


// hold of on this test... may not be necessary depending on implementation.  Plan out implementation first.

test('parseDeck should parse decks and return an array of card names as a result', function (assert) {

  let controller = this.subject();
  assert.expect(1);
  controller.parseDeck("3 Jace, the Mind Sculptor\n4 Island\n2 Counterspell\n\n", "3 Swamp\n3 Dark Confidant");

  controller.set("queryParams", "decklist1=3 Jace, the Mind Sculptor\n4 Island\n2 Counterspell\n\n");
  
  assert.equal(controller.get("queryParams"), "3 Jace, the Mind Sculptor\n4 Island\n2 Counterspell\n\n");


});


/*
1. matchup/ will have query params for decklists.  This will be a string that is placed in a textarea
2. decklist1 will go in decklist 1 text area, same for decklist 2

*/



